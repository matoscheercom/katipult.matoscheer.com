<?php

abstract class BasePresenter extends \Nette\Application\UI\Presenter {

    public $lang = 'en';

    public function beforeRender() {
        parent::beforeRender();
        switch($this->lang) {
            case 'en':
            case 'fr':
               $this->template->lang = $this->lang;
                break;
            default:
                $this->redirect('this',array('lang' => 'en'));
                $this->terminate();
        }
        $tm = $this->context->getParameters();

        $this->template->meta = $tm['meta'][$this->lang];
        $this->template->navigation = $tm['navigation'][$this->lang];
        $this->template->header = $tm['header'][$this->lang];
        $this->template->video = $tm['video'][$this->lang];
        $this->template->account = $tm['account'][$this->lang];
        $this->template->partners = $tm['partners'][$this->lang];
        $this->template->networks = $tm['networks'][$this->lang];
        $this->template->footer = $tm['footer'][$this->lang];
    }

} 