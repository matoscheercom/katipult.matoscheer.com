<?php

/**
 * @author Michal Bystricky <michal.bystricky@icloud.com>
 */
use Nette\Configurator;

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Configurator;
$environmentFile = __DIR__ . '/configuration/environment';

$debugMode = file_exists(__DIR__ . '/../configuration/environment') ? true : false;

umask(0002);

$configurator->setDebugMode($debugMode);
$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator
    ->createRobotLoader()
    ->addDirectory(__DIR__)
    ->register();

$configurator->addConfig(__DIR__ . '/../configuration/application.neon', Configurator::AUTO);

if ($debugMode === true) {
    // Development mode
    $configurator->addConfig(__DIR__ . '/../configuration/development.neon');
} else {
    // Production mode
    $configurator->addConfig(__DIR__ . '/../configuration/production.neon');
}

$container = $configurator->createContainer();
return $container;