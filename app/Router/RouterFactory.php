<?php

use Nette\Object;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

final class RouterFactory extends Object {

    /**
     * @return RouteList
     */
    public function createRouter() {
        $router = new RouteList();

        $router[] = new Route('/', 'Homepage:default');

        return $router;
    }
} 