// - functions declarations - //

// return offset width of elements
var calculateBtnW = function(windowW,constant,windowDiv,constantDiv) {
    newW = (windowW / windowDiv) - (constant / constantDiv);
    if (newW <= constant) {
        newW = 0;
    }
    else {
        newW = newW;
    }
    return newW;
};




(function() {
    App = function() {

        // - variable and constants declarations - //
        var btn = jQuery("#carousel-btn");
        var btnC = jQuery(".js-collapse");
        var headerEl = jQuery('.js-header');
        var bodyEl = jQuery('body');



        const headerBtnimageW = 46;


        // - app body - //

        // rollup header
        var rollUpHandler = function() {
            btn.on('click',function(e) {
                e.preventDefault();
                timeout = btn.data('timeout');
                headerEl.toggleClass('collapse-in');
                jQuery('html, body').animate({scrollTop: 0 }, timeout);
                setTimeout(function() {
                    bodyEl.toggleClass('fixed');
                },timeout);
            });
        };

        // header button offsets handler
        var headerBtnHandler = function() {
            vieportW = jQuery(window).width();
            calculateBtnW(vieportW,headerBtnimageW,2,2);
            btn.find('span').css('width', newW + 'px');
        };

        // collapse navigation handler
        var collapseHandler = function() {
            btnC.on('click',function(e) {
                e.preventDefault();
                var targetC = btnC.data('target');
                jQuery('.' + targetC).toggleClass('rollout');
            });
        };

        return {
            init: function() {
                jQuery(document).ready(function() {
                    headerBtnHandler();
                    rollUpHandler();
                    collapseHandler();
                });
                jQuery(window).resize(function() {
                    headerBtnHandler();
                });
            }
        }
    }();

    App.init();

})();